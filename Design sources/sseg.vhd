-- Floating point adder
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity fpadder_basys3_interface is
    Port ( 
           --reset : in STD_LOGIC; -- reset
           clk : in STD_LOGIC;-- 100Mhz clock on Basys 3 FPGA board
           Anode_Activate : out STD_LOGIC_VECTOR (3 downto 0);-- 4 Anode signals
           SSEG_LED : out STD_LOGIC_VECTOR (6 downto 0); -- Cathode patterns of 7-segment display
           LED : out std_logic_vector(15 downto 0);
           SW  : in  STD_LOGIC_VECTOR (15 downto 0);
           BTN : in  STD_LOGIC_VECTOR (4 downto 0)
           );
end fpadder_basys3_interface;

architecture Behavioral of fpadder_basys3_interface is

component debouncer
Generic(
        DEBNC_CLOCKS : integer;
        PORT_WIDTH : integer);
Port(
		SIGNAL_I : in std_logic_vector(4 downto 0);
		CLK_I : in std_logic;          
		SIGNAL_O : out std_logic_vector(4 downto 0)
		);
end component;

component fpadder_core is
--  Port ( );
    Port(
        a : in std_logic_vector(31 downto 0);
        b : in std_logic_vector(31 downto 0);
        clk : in std_logic;
        reset : in std_logic;
        start : in std_logic;
        result : out std_logic_vector(31 downto 0);
        done : out std_logic
--        expeq_out : out std_logic;
--        swap_out : out std_logic;
--        greater_out : out std_logic_vector(1 downto 0);
--        num1_out : out std_logic_vector(33 downto 0);
--        num2_out : out std_logic_vector(33 downto 0);
--        numres_out : out std_logic_vector(33 downto 0);
--        expeq_state_out : out std_logic_vector(4 downto 0);
--        fpadder_state_out : out std_logic_vector(4 downto 0);
--        expeq_num1_exp_out : out std_logic_vector(7 downto 0);
--        expeq_num2_exp_out : out std_logic_vector(7 downto 0);
--        expeq_num1_mant_out : out std_logic_vector(24 downto 0);
--        expeq_num2_mant_out : out std_logic_vector(24 downto 0);
--        norm_state_out : out std_logic_vector(4 downto 0);
--        norm_numres_exp_out : out std_logic_vector(7 downto 0);
--        norm_numres_mant_out : out std_logic_vector(24 downto 0)
    );
end component;

--Used to determine when a button press has occured
signal btnReg : std_logic_vector (4 downto 0) := "00000";
signal btnDetect : std_logic;

--Debounced btn signals used to prevent single button presses
--from being interpreted as multiple button presses.
signal btnDeBnc : std_logic_vector(4 downto 0);

signal rst, start, loadip, seeip, seeop : std_logic;

signal a : std_logic_vector(31 downto 0);
signal b : std_logic_vector(31 downto 0);
signal fp_rst, fp_done, fp_start : std_logic;
signal result : std_logic_vector(31 downto 0);

-- the text to be displayed on the SSEG
signal displayed_number: STD_LOGIC_VECTOR (15 downto 0);
-- counting decimal number to be displayed on 4-digit 7-segment display
signal LED_BCD: STD_LOGIC_VECTOR (3 downto 0);
signal refresh_counter: STD_LOGIC_VECTOR (19 downto 0);
-- creating 10.5ms refresh period
signal LED_activating_counter: std_logic_vector(1 downto 0);
-- the other 2-bit for creating 4 LED-activating signals
-- count         0    ->  1  ->  2  ->  3
-- activates    LED1    LED2   LED3   LED4
-- and repeat

subtype statetype is std_logic_vector(4 downto 0);
constant fsm_state_initial : statetype := "00000";
constant fsm_state_reset   : statetype := "00001";
constant fsm_state_load_inputs1: statetype := "00010";
constant fsm_state_load_inputs2: statetype := "00011";
constant fsm_state_load_inputs3: statetype := "00100";
constant fsm_state_load_inputs4: statetype := "00101";
constant fsm_state_reset_fpadder: statetype := "00110";
constant fsm_state_start_fpadder:  statetype := "00111";
constant fsm_state_wait_fpadder_done:  statetype := "01000";
constant fsm_state_show_op1: statetype := "01001";
constant fsm_state_show_op2: statetype := "01010";
constant mode_ipload : std_logic := '0';
constant mode_showop : std_logic := '1';

-- Final FSM
signal fsm_present_state : statetype;
signal fsm_loadip_state : statetype;
signal fsm_showop_state : statetype;
signal fsm_mode : std_logic;

begin

--Debounces btn signals
Inst_btn_debounce: debouncer 
    generic map(
        DEBNC_CLOCKS => (2**16),
        PORT_WIDTH => 5)
    port map(
		SIGNAL_I => BTN,
		CLK_I => clk,
		SIGNAL_O => btnDeBnc
	);
fpcore : fpadder_core port map (a=>a,b=>b,clk=>clk,reset=>fp_rst,start=>fp_start, done=>fp_done, result=>result);

--Registers the debounced button signals, for edge detection.
btn_reg_process : process (clk)
begin
	if (rising_edge(clk)) then
		btnReg <= btnDeBnc(4 downto 0);
	end if;
end process;

button_signal_register : process (clk)
begin
	if (rising_edge(clk)) then
		if ((btnReg(4)='0' and btnDeBnc(4)='1')) then
			rst <= '1';
			start <= '0';
			loadip <= '0';
			seeip <= '0';
			seeop <= '0';
	    elsif ((btnReg(0)='0' and btnDeBnc(0)='1')) then
			rst <= '0';
            start <= '1';
            loadip <= '0';
            seeip <= '0';
            seeop <= '0';
	    elsif ((btnReg(1)='0' and btnDeBnc(1)='1')) then
            rst <= '0';
            start <= '0';
            loadip <= '0';
            seeip <= '1';
            seeop <= '0';
	    elsif ((btnReg(2)='0' and btnDeBnc(2)='1')) then
            rst <= '0';
            start <= '0';
            loadip <= '1';
            seeip <= '0';
            seeop <= '0';
	    elsif ((btnReg(3)='0' and btnDeBnc(3)='1')) then
            rst <= '0';
            start <= '0';
            loadip <= '0';
            seeip <= '0';
            seeop <= '1';
	    else
	        rst <= '0';
	        start <= '0';
            loadip <= '0';
            seeip <= '0';
            seeop <= '0';
	    end if;
	end if;
end process;

main_fsm : process (clk)
begin
    if (rising_edge(clk)) then
        if (rst = '1') then
            displayed_number <= x"0000";
            LED <= x"0000";
            fsm_present_state <= fsm_state_load_inputs1;
            fsm_loadip_state <= fsm_state_load_inputs1;
            fsm_showop_state <= fsm_state_show_op1;
            a <= x"00000000";
            b <= x"00000000";
--            result <= x"00000000";
            fp_rst <= '0';
            fp_start <= '0';
            fsm_mode <= mode_ipload;
        else
             case fsm_present_state is
                -- Input load state machine below :
                when fsm_state_load_inputs1 =>
                    displayed_number <= x"50A1";
                    LED <= a(31 downto 16);
                    if (loadip = '1' or seeip = '1') then
                        if (loadip = '1') then
                            a(31 downto 16) <= SW;
                        end if;
                        fsm_present_state <= fsm_state_load_inputs2;
                    elsif (seeop = '1') then
                        fsm_present_state <= fsm_state_show_op1;
                    elsif (start = '1') then
                        fsm_present_state <= fsm_state_reset_fpadder;
                    else 
                        fsm_present_state <= fsm_state_load_inputs1;
                    end if;
                    
                 when fsm_state_load_inputs2 =>
                     displayed_number <= x"50A2";
                     LED <= a(15 downto 0);
                     if (loadip = '1' or seeip = '1') then
                         if (loadip = '1') then
                             a(15 downto 0) <= SW;
                         end if;
                         fsm_present_state <= fsm_state_load_inputs3;
                     elsif (seeop = '1') then
                         fsm_present_state <= fsm_state_show_op1;
                     elsif (start = '1') then
                         fsm_present_state <= fsm_state_reset_fpadder;
                     else
                         fsm_present_state <= fsm_state_load_inputs2;
                     end if;  
                                           
                 when fsm_state_load_inputs3 =>
                     displayed_number <= x"50B1";
                     LED <= b(31 downto 16);
                     if (loadip = '1' or seeip = '1') then
                         if (loadip = '1') then
                             b(31 downto 16) <= SW;
                         end if;
                         fsm_present_state <= fsm_state_load_inputs4;
                     elsif (seeop = '1') then
                         fsm_present_state <= fsm_state_show_op1;
                     elsif (start = '1') then
                         fsm_present_state <= fsm_state_reset_fpadder;
                     else
                         fsm_present_state <= fsm_state_load_inputs3;
                     end if; 
                     
                  when fsm_state_load_inputs4 =>
                     displayed_number <= x"50B2";
                     LED <= b(15 downto 0);
                     if (loadip = '1' or seeip = '1') then
                         if (loadip = '1') then
                             b(15 downto 0) <= SW;
                         end if;
                         fsm_present_state <= fsm_state_load_inputs1;
                     elsif (seeop = '1') then
                         fsm_present_state <= fsm_state_show_op1;
                     elsif (start = '1') then
                         fsm_present_state <= fsm_state_reset_fpadder;
                     else
                         fsm_present_state <= fsm_state_load_inputs4;
                     end if;
                     
                  -- Computation & output display state machine below :
                  when fsm_state_reset_fpadder =>
                      fp_rst <= '1';
                      fp_start <= '0';
                      fsm_present_state <= fsm_state_start_fpadder;
                      
                  when fsm_state_start_fpadder=>
                      fp_rst <= '0';
                      fp_start <= '1';
                      fsm_present_state <= fsm_state_wait_fpadder_done;
                  
                  when fsm_state_wait_fpadder_done =>
                      fp_start <= '0';
                      if (fp_done = '1') then
                          fsm_present_state <= fsm_state_show_op1;
                      else
                          fsm_present_state <= fsm_state_wait_fpadder_done;
                      end if;
                      
                  when fsm_state_show_op1 =>
                      displayed_number <= x"FE01";
                      LED <= result (31 downto 16);
                      if (seeop = '1') then
                          fsm_present_state <= fsm_state_show_op2;
                      elsif (loadip = '1' or seeip = '1') then
                          fsm_present_state <= fsm_state_load_inputs1;
                      else
                          fsm_present_state <= fsm_state_show_op1;
                      end if;
                      
                  when fsm_state_show_op2 =>
                      displayed_number <= x"FE02";
                      LED <= result (15 downto 0);
                      if (seeop = '1') then
                          fsm_present_state <= fsm_state_show_op1;
                      elsif (loadip = '1' or seeip = '1') then
                          fsm_present_state <= fsm_state_load_inputs1;
                      else
                          fsm_present_state <= fsm_state_show_op2;
                      end if;
                  when others =>
                      fsm_present_state <= fsm_state_load_inputs1;
             end case;
        end if;
    end if;
end process;

-- VHDL code for BCD to 7-segment decoder
-- Cathode patterns of the 7-segment LED display 
process(LED_BCD)
begin
    case LED_BCD is
    when "0000" => SSEG_LED <= "1111110"; -- "-" for "0"   
    when "0001" => SSEG_LED <= "1001111"; -- "1" 
    when "0010" => SSEG_LED <= "0010010"; -- "2" 
    when "0011" => SSEG_LED <= "0000110"; -- "3" 
    when "0100" => SSEG_LED <= "1001100"; -- "4" 
    when "0101" => SSEG_LED <= "1110001"; -- "L" for "5" 
    when "0110" => SSEG_LED <= "0100000"; -- "6" 
    when "0111" => SSEG_LED <= "0001111"; -- "7" 
    when "1000" => SSEG_LED <= "0000000"; -- "8"     
    when "1001" => SSEG_LED <= "0000100"; -- "9" 
    when "1010" => SSEG_LED <= "0000010"; -- a
    when "1011" => SSEG_LED <= "1100000"; -- b
    when "1100" => SSEG_LED <= "0110001"; -- C
    when "1101" => SSEG_LED <= "1000010"; -- d
    when "1110" => SSEG_LED <= "1000001"; -- E for "U"
    when "1111" => SSEG_LED <= "0000001"; -- F for "0"
    end case;
end process;
-- 7-segment display controller
-- generate refresh period of 10.5ms
process(clk,rst)
begin 
    if(rst='1') then
        refresh_counter <= (others => '0');
    elsif(rising_edge(clk)) then
        refresh_counter <= refresh_counter + 1;
    end if;
end process;
 LED_activating_counter <= refresh_counter(19 downto 18);
-- 4-to-1 MUX to generate anode activating signals for 4 LEDs 
process(LED_activating_counter)
begin
    case LED_activating_counter is
    when "00" =>
        Anode_Activate <= "0111"; 
        -- activate LED1 and Deactivate LED2, LED3, LED4
        LED_BCD <= displayed_number(15 downto 12);
        -- the first hex digit of the 16-bit number
    when "01" =>
        Anode_Activate <= "1011"; 
        -- activate LED2 and Deactivate LED1, LED3, LED4
        LED_BCD <= displayed_number(11 downto 8);
        -- the second hex digit of the 16-bit number
    when "10" =>
        Anode_Activate <= "1101"; 
        -- activate LED3 and Deactivate LED2, LED1, LED4
        LED_BCD <= displayed_number(7 downto 4);
        -- the third hex digit of the 16-bit number
    when "11" =>
        Anode_Activate <= "1110"; 
        -- activate LED4 and Deactivate LED2, LED3, LED1
        LED_BCD <= displayed_number(3 downto 0);
        -- the fourth hex digit of the 16-bit number    
    end case;
end process;
-- Counting the number to be displayed on 4-digit 7-segment Display 
-- on Basys 3 FPGA board

end Behavioral;
