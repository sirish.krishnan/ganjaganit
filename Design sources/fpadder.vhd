----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/27/2018 02:32:45 PM
-- Design Name: 
-- Module Name: fpadder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fpadder_core is
--  Port ( );
    Port(
        a : in std_logic_vector(31 downto 0);
        b : in std_logic_vector(31 downto 0);
        clk : in std_logic;
        reset : in std_logic;
        start : in std_logic;
        result : out std_logic_vector(31 downto 0);
        done : out std_logic
--        expeq_out : out std_logic;
--        swap_out : out std_logic;
--        greater_out : out std_logic_vector(1 downto 0);
--        num1_out : out std_logic_vector(33 downto 0);
--        num2_out : out std_logic_vector(33 downto 0);
--        numres_out : out std_logic_vector(33 downto 0);
--        expeq_state_out : out std_logic_vector(4 downto 0);
--        fpadder_state_out : out std_logic_vector(4 downto 0);
--        expeq_num1_exp_out : out std_logic_vector(7 downto 0);
--        expeq_num2_exp_out : out std_logic_vector(7 downto 0);
--        expeq_num1_mant_out : out std_logic_vector(24 downto 0);
--        expeq_num2_mant_out : out std_logic_vector(24 downto 0);
--        norm_state_out : out std_logic_vector(4 downto 0);
--        norm_numres_exp_out : out std_logic_vector(7 downto 0);
--        norm_numres_mant_out : out std_logic_vector(24 downto 0)
    );
end fpadder_core;

architecture Behavioral of fpadder_core is

-- State machine state definitions
-- For main FSM
subtype statetype is std_logic_vector(4 downto 0);
constant fpadder_state_initial : statetype := "00000";
constant fpadder_state_take_inputs : statetype := "00001";
constant fpadder_state_validate_input : statetype := "00010";
constant fp_adder_state_load_extended_inputs : statetype := "00011";
constant fp_adder_state_reset_expeq : statetype := "00100";
constant fp_adder_state_start_expeq : statetype := "00101";
constant fp_adder_state_wait_till_expeq_ready : statetype := "00110";
constant fp_adder_state_do_add : statetype := "00111";
constant fp_adder_reset_norm : statetype := "01000";
constant fp_adder_start_norm : statetype := "01001";
constant fp_adder_wait_norm_done : statetype := "01010";
constant fp_adder_state_done : statetype := "11111"; 
constant zero_mantissa : std_logic_vector(22 downto 0) := (others => '0');
constant zero_extended_mantissa : std_logic_vector(24 downto 0) := (others => '0');
constant all_zeros : std_logic_vector(31 downto 0) := (others => '0');

-- For exponent equalizer
constant expeq_state_initial : statetype := "00000";
constant expeq_state_start : statetype := "00001";
constant expeq_state_right_shift_num2exp : statetype := "00010";
constant expeq_state_done : statetype := "11111";

-- For result normalizer
constant norm_state_initial : statetype := "00000";
constant norm_state_start : statetype := "00001";
constant norm_state_check_cases : statetype := "00010";
constant norm_state_left_shift_till_normal : statetype := "00011";
constant norm_state_check_overflow : statetype := "00100";
constant norm_state_done : statetype := "11111";

-- Signals for the input number's temporary buffer storage
signal a_buffer, b_buffer : std_logic_vector(31 downto 0);
signal num1, num2, numres : std_logic_vector(33 downto 0);
signal fpadder_state : statetype;
signal a_sign, b_sign : std_logic;
signal a_exp, b_exp : std_logic_vector(7 downto 0);
signal a_mant, b_mant : std_logic_vector(22 downto 0);

-- Signals for check_input_magnitude process
signal expeq,swap : std_logic;
signal greater    : std_logic_vector(1 downto 0);

-- Signals for the exponent equalizer FSM
signal expeq_reset, expeq_start, expeq_done : std_logic;
signal expeq_state : statetype;
signal expeq_overflow : std_logic;
signal expeq_num1_exp, expeq_num2_exp : std_logic_vector(7 downto 0);
signal expeq_num1_mant, expeq_num2_mant : std_logic_vector(24 downto 0);
signal expeq_right_shift_count : std_logic_vector(7 downto 0);

-- Signals for the result normalizer FSM
signal norm_reset, norm_start, norm_done : std_logic;
signal norm_state : statetype;
signal norm_overflow : std_logic;
signal norm_num1_exp, norm_num2_exp, norm_numres_exp : std_logic_vector(7 downto 0);
signal norm_num1_mant, norm_num2_mant, norm_numres_mant : std_logic_vector(24 downto 0);

begin

-- Get the sign, exponent & mantissa bits of 'a' & 'b'
a_sign <= a_buffer(31);
b_sign <= b_buffer(31);
a_exp  <= a_buffer(30 downto 23);
b_exp  <= b_buffer(30 downto 23);
a_mant  <= a_buffer(22 downto 0);
b_mant  <= b_buffer(22 downto 0);

--Get the sign, exponent & mantissa parts of num1 & num2 

-- Debug output state
--expeq_num1_exp_out <= expeq_num1_exp;
--expeq_num2_exp_out <= expeq_num2_exp;
--expeq_num1_mant_out <= expeq_num1_mant;
--expeq_num2_mant_out <= expeq_num2_mant;
--fpadder_state_out <= fpadder_state;
--expeq_state_out <= expeq_state;
--expeq_out <= expeq;
--swap_out <= swap;
--greater_out <= greater;
--num1_out <= num1;
--num2_out <= num2;
--numres_out <= numres;
--norm_state_out <= norm_state;
--norm_numres_mant_out <= norm_numres_mant;
--norm_numres_exp_out <= norm_numres_exp;

-- State machine to equalize exponents of 2 numbers
fpadder_normalize_result: process(clk)
begin
    if (rising_edge(clk)) then
        if (norm_reset = '1') then
            norm_state <= norm_state_initial;
            norm_done <= '0';
            norm_num1_exp <= (others => '0');
            norm_num2_exp <= (others => '0');
            norm_numres_exp <= (others => '0');
            norm_num1_mant <= (others => '0');
            norm_num2_mant <= (others => '0');
            norm_numres_mant <= (others => '0');
        else
            case norm_state is
            
                when norm_state_initial =>
                    if (norm_start = '1') then
                        norm_state <= norm_state_start;
                    else
                        norm_state <= norm_state_initial;
                    end if;
                    
                when norm_state_start =>
                    norm_num1_exp <= num1(32 downto 25);
                    norm_num2_exp <= num2(32 downto 25);
                    norm_numres_exp <= numres(32 downto 25);
                    norm_num1_mant <= num1(24 downto 0);
                    norm_num2_mant <= num2(24 downto 0);
                    norm_numres_mant <= numres(24 downto 0);
                    norm_state <= norm_state_check_cases;

                when norm_state_check_cases =>
                    -- Check if result is all 0's
                    if (norm_numres_mant = zero_extended_mantissa) then
                        norm_numres_exp <= x"00";
                        norm_state <= norm_state_done;
                    -- If the numbers were subnormal to begin with,
                    -- handle the case where a carry was generated to 
                    -- the MSB & make the result normal
                    elsif (norm_num1_exp = x"00" ) then
                        if (norm_numres_mant(23) = '1') then
                            norm_numres_exp <= x"01";
                        end if;
                        norm_state <= norm_state_done;
                    -- For numbers not in the subnormal range
                    else
                       -- Handle the case where a carry was generated from
                       -- the normal bit position itself
                       if (norm_numres_mant(24) = '1') then
                           norm_numres_mant <= std_logic_vector(shift_right(unsigned(norm_numres_mant),1));
                           norm_numres_exp <= norm_numres_exp + 1;
                           norm_state <= norm_state_check_overflow;
                       -- Case when result is not normalized
                       elsif (norm_numres_mant(24 downto 23) = "00") then
                           norm_state <= norm_state_left_shift_till_normal;
                       else
                           norm_state <= norm_state_done;
                       end if;
                    end if;
                    
                when norm_state_left_shift_till_normal =>
                    if (norm_numres_exp = x"01") then
                        norm_numres_exp <= x"00";
                        norm_state <= norm_state_done;
                    elsif (norm_numres_mant(23) = '0') then
                        norm_numres_mant <= std_logic_vector(shift_left(unsigned(norm_numres_mant),1));
                        norm_numres_exp <= norm_numres_exp - 1;
                        norm_state <= norm_state_left_shift_till_normal;
                    else
                        norm_state <= norm_state_done; 
                    end if;

                when norm_state_check_overflow =>
                -- If exponent is 255, set the mantissa to 0's to indicate
                -- + / - Inf
                    if (norm_numres_exp = x"FF") then
                        norm_numres_mant <= (others => '0');
                    end if;
                    norm_state <= norm_state_done;

                when norm_state_done =>
                    norm_done <= '1';
                    norm_state <= norm_state_done;
                    
                when others =>
                    norm_state <= norm_state_initial;
                 
            end case;
        end if;
    end if;
end process;

-- State machine to equalize exponents of 2 numbers
fpadder_make_exp_equal: process(clk)
begin
    if (rising_edge(clk)) then
        if (expeq_reset = '1') then
            expeq_state <= expeq_state_initial;
            expeq_done <= '0';
            expeq_overflow <= '0';
            expeq_right_shift_count <= (others => '0');
            expeq_num1_exp <= (others => '0');
            expeq_num2_exp <= (others => '0');
            expeq_num1_mant <= (others => '0');
            expeq_num2_mant <= (others => '0');
        else
            case expeq_state is
            
                when expeq_state_initial =>
                    if (expeq_start = '1') then
                        expeq_num1_exp <= num1(32 downto 25);
                        expeq_num2_exp <= num2(32 downto 25);
                        expeq_num1_mant <= num1(24 downto 0);
                        expeq_num2_mant <= num2(24 downto 0);
                        expeq_state <= expeq_state_start;
                    else
                        expeq_state <= expeq_state_initial;
                    end if;
                    
                when expeq_state_start =>
                    expeq_num1_mant(23) <= '1';
                    -- If num2 is normalized version
                    if (expeq_num2_exp > 0) then
                        expeq_num2_mant(23) <= '1';
                        expeq_right_shift_count <= expeq_num1_exp - expeq_num2_exp;
                    -- If num2 is denormalized version    
                    else
                        expeq_num2_mant(23) <= '0';
                        expeq_num2_exp <= expeq_num2_exp + 1;
                        expeq_right_shift_count <= expeq_num1_exp - expeq_num2_exp - 1;
                    end if;
                    expeq_state <= expeq_state_right_shift_num2exp;
               
                when expeq_state_right_shift_num2exp =>               
                    if (expeq_right_shift_count = x"00") then
                        expeq_state <= expeq_state_done;
                    else
                        expeq_num2_mant <= std_logic_vector(shift_right(unsigned(expeq_num2_mant),1));
                        expeq_num2_exp <= expeq_num2_exp + 1;
                        expeq_right_shift_count <= expeq_right_shift_count - 1;
                        expeq_state <= expeq_state_right_shift_num2exp;
                    end if;
                    
                when expeq_state_done =>
                    expeq_done <= '1';
                    expeq_state <= expeq_state_done;
                    
                when others =>
                    expeq_state <= expeq_state_initial;
                 
            end case;
        end if;
    end if;
end process;

-- Combinational logic to determine which of the 2 inputs
-- are equal and assert flags based on the comparison tests
check_input_magnitude:process(a_exp,b_exp,a_mant,b_mant)
begin
    if (a_exp > b_exp) then
        expeq <= '0';
        swap <= '0';
        greater <= "10";
    elsif (b_exp > a_exp) then
        expeq <= '0';
        swap <= '1';
        greater <= "01";
    elsif (a_mant > b_mant) then
        expeq <= '1';
        swap <= '0';
        greater <= "10";
    elsif (b_mant > a_mant) then
        expeq <= '1';
        swap <= '1';
        greater <= "01"; 
    else 
        expeq <= '1';
        swap <= '0';
        greater <= "11"; 
    end if;
end process;

-- Main FSM of the system
fpadder_main_fsm: process(clk)
begin
    if (rising_edge(clk)) then
        if (reset = '1') then
            result <= (others => '0');
            done <= '0';
            fpadder_state <= fpadder_state_initial;
            a_buffer <= all_zeros;
            b_buffer <= all_zeros;
            num1 <= (others => '0');
            num2 <= (others => '0');
            numres <= (others => '0');
            expeq_reset <= '0';
            expeq_start <= '0';
            norm_reset <= '0';  
            norm_start <= '0';
        else
            case fpadder_state is
            
            -- Check if 'start' signal has been enabled / not
                when  fpadder_state_initial =>
                    if (start = '1') then
                        fpadder_state <= fpadder_state_take_inputs;
                    else
                        fpadder_state <= fpadder_state_initial;
                    end if;
                
            -- Buffer the inputs        
                when fpadder_state_take_inputs =>
                    a_buffer <= a;
                    b_buffer <= b;
                    fpadder_state <= fpadder_state_validate_input;
                    
            -- Input validation check for 0's, Inf & NaN values
                when fpadder_state_validate_input =>
                    -- Check if either a or b is all 0's
                    if (a_buffer = all_zeros or b_buffer = all_zeros) then
                        result <= a_buffer + b_buffer;
                        fpadder_state <= fp_adder_state_done;
                    elsif (a_exp = x"FF" and a_mant = zero_mantissa) then
                        result <= a_buffer;
                        fpadder_state <= fp_adder_state_done;
                    elsif (b_exp = x"FF" and b_mant = zero_mantissa) then
                        result <= b_buffer;
                        fpadder_state <= fp_adder_state_done;
                    elsif (a_exp = x"FF" and a_mant /= zero_mantissa) then
                        result <= a_buffer;
                        fpadder_state <= fp_adder_state_done;
                    elsif (b_exp = x"FF" and b_mant /= zero_mantissa) then
                        result <= b_buffer;
                        fpadder_state <= fp_adder_state_done;
                    else
                        fpadder_state <= fp_adder_state_load_extended_inputs;
                    end if; 
            
            -- Equalize a's and b's exponents
                when fp_adder_state_load_extended_inputs =>
                    if (swap = '1') then
                        num1(33) <= b_buffer(31);
                        num1(32 downto 25) <= b_buffer(30 downto 23);
                        num1(24 downto 23)<= "00";
                        num1(22 downto 0) <= b_buffer(22 downto 0);
                        num2(33) <= a_buffer(31);
                        num2(32 downto 25) <= a_buffer(30 downto 23);
                        num2(24 downto 23)<= "00";
                        num2(22 downto 0) <= a_buffer(22 downto 0);
                    else
                        num2(33) <= b_buffer(31);
                        num2(32 downto 25) <= b_buffer(30 downto 23);
                        num2(24 downto 23)<= "00";
                        num2(22 downto 0) <= b_buffer(22 downto 0);
                        num1(33) <= a_buffer(31);
                        num1(32 downto 25) <= a_buffer(30 downto 23);
                        num1(24 downto 23)<= "00";
                        num1(22 downto 0) <= a_buffer(22 downto 0);
                    end if;
                    if (expeq = '1') then
                        fpadder_state <= fp_adder_state_do_add;
                    else    
                        fpadder_state <= fp_adder_state_reset_expeq;
                    end if;
                    
                when fp_adder_state_reset_expeq =>
                    expeq_reset <= '1';
                    fpadder_state <= fp_adder_state_start_expeq;
            
                when fp_adder_state_start_expeq =>
                    expeq_reset <= '0';
                    expeq_start  <= '1';
                    fpadder_state <= fp_adder_state_wait_till_expeq_ready;
            
                when fp_adder_state_wait_till_expeq_ready =>
                    expeq_start <= '0';
                    if (expeq_done = '1') then
                        num1(32 downto 25) <= expeq_num1_exp;
                        num1(24 downto 0)<= expeq_num1_mant;
                        num2(32 downto 25) <= expeq_num2_exp;
                        num2(24 downto 0)<= expeq_num2_mant;
                        fpadder_state <= fp_adder_state_do_add;
                    else
                        fpadder_state <= fp_adder_state_wait_till_expeq_ready;
                    end if;
            
            -- Perform addition / subtraction on the mantissa based on sign bits
                when fp_adder_state_do_add =>
                    if (num1(33) = num2(33)) then
                        numres(24 downto 0) <= num1(24 downto 0) + num2(24 downto 0);
                    else
                        numres(24 downto 0) <= num1(24 downto 0) - num2(24 downto 0);
                    end if;
                    -- Set sign bit & exponent of result to that of num1
                    numres (33) <= num1 (33);
                    numres (32 downto 25) <= num1(32 downto 25);
                    fpadder_state <= fp_adder_reset_norm;
                
                when fp_adder_reset_norm =>
                    norm_reset <= '1'; 
                    fpadder_state <= fp_adder_start_norm;        
                
            -- Normalize the result
                when fp_adder_start_norm =>
                    norm_reset <= '0';
                    norm_start <= '1';
                    fpadder_state <= fp_adder_wait_norm_done;
                
                when fp_adder_wait_norm_done =>
                    norm_start <= '0';
                    if (norm_done = '1') then
                        result(31) <= num1(33);
                        result(30 downto 23) <= norm_numres_exp;
                        result(22 downto 0) <= norm_numres_mant(22 downto 0);
                        fpadder_state <= fp_adder_state_done;
                    else
                        fpadder_state <= fp_adder_wait_norm_done;
                    end if;
                    
            -- Set the 'done' signal to true        
                when fp_adder_state_done => 
                    done <= '1';
                    fpadder_state <=  fp_adder_state_done;
                
                when others =>
                    fpadder_state <= fpadder_state_initial;
            end case;
        end if;
    end if;
end process;

end Behavioral;
