----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/27/2018 03:14:20 PM
-- Design Name: 
-- Module Name: test_fpadder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test_fpadder is
--  Port ( );
end test_fpadder;

architecture Behavioral of test_fpadder is

component fpadder_core is
--  Port ( );
    Port(
        a : in std_logic_vector(31 downto 0);
        b : in std_logic_vector(31 downto 0);
        clk : in std_logic;
        reset : in std_logic;
        start : in std_logic;
        result : out std_logic_vector(31 downto 0);
        done : out std_logic;
        expeq_out : out std_logic;
        swap_out : out std_logic;
        greater_out : out std_logic_vector(1 downto 0);
        num1_out : out std_logic_vector(33 downto 0);
        num2_out : out std_logic_vector(33 downto 0);
        numres_out : out std_logic_vector(33 downto 0);
        expeq_state_out : out std_logic_vector(4 downto 0);
        fpadder_state_out : out std_logic_vector(4 downto 0);
        expeq_num1_exp_out : out std_logic_vector(7 downto 0);
        expeq_num2_exp_out : out std_logic_vector(7 downto 0);
        expeq_num1_mant_out : out std_logic_vector(24 downto 0);
        expeq_num2_mant_out : out std_logic_vector(24 downto 0);
        norm_state_out : out std_logic_vector(4 downto 0);
        norm_numres_exp_out : out std_logic_vector(7 downto 0);
        norm_numres_mant_out : out std_logic_vector(24 downto 0)
    );
end component;

-- Constant declarations
constant period: time := 10 ns;
constant duty_cycle: real := 0.5;
constant offset: time := 120 ns;
constant setup: time := 2 ns;
constant tco: time := 3 ns;
constant onerun: time := 120 ns;

-- Signal declarations
signal num1_out,num2_out,numres_out : std_logic_vector(33 downto 0);
signal a,b,result : std_logic_vector(31 downto 0);
signal fpadder_state_out : std_logic_vector(4 downto 0);
signal clk, reset, start, done,expeq_out,swap_out : std_logic;
signal greater_out : std_logic_vector(1 downto 0);
signal expeq_state_out : std_logic_vector(4 downto 0);
signal expeq_num1_exp_out : std_logic_vector(7 downto 0);
signal expeq_num2_exp_out : std_logic_vector(7 downto 0);
signal expeq_num1_mant_out : std_logic_vector(24 downto 0);
signal expeq_num2_mant_out : std_logic_vector(24 downto 0);
signal norm_state_out : std_logic_vector(4 downto 0);
signal norm_numres_exp_out : std_logic_vector(7 downto 0);
signal norm_numres_mant_out : std_logic_vector(24 downto 0);

begin

fpcore : fpadder_core port map (norm_numres_mant_out=>norm_numres_mant_out,norm_numres_exp_out=>norm_numres_exp_out,norm_state_out=>norm_state_out, 
numres_out=>numres_out,expeq_num1_mant_out=>expeq_num1_mant_out, expeq_num2_mant_out=>expeq_num2_mant_out, expeq_num1_exp_out=>expeq_num1_exp_out, expeq_num2_exp_out=>expeq_num2_exp_out, expeq_state_out => expeq_state_out, num1_out=>num1_out,num2_out=>num2_out,expeq_out=>expeq_out,swap_out=>swap_out,greater_out=>greater_out,
a=>a,b=>b,reset=>reset,clk=>clk,result => result, done=>done,start=>start,fpadder_state_out=>fpadder_state_out);

clock: process -- clock process for clk
begin
wait for offset;
    clock_loop : loop
        clk <= '0';
        wait for (period - (period * duty_cycle));
        clk <= '1'; wait for (period * duty_cycle);
    end loop clock_loop;
end process;

test_process:process
begin
    wait for offset;
    wait for (period - (period * duty_cycle) -
    setup);
        reset <= '1';
    wait for period;
        reset <= '0';
        start <= '0';
--        a <= x"ff800000";
--        b <= x"09002233";
--        a <= x"09002233";
--        b <= x"ff800000";
--        a <= x"00778899";
--        b <= x"00000000";
--        a <= x"00000000";
--        b <= x"00899900";
--        a <= x"ff8ff000";
--        b <= x"09002233";
--        a <= x"09002233";
--        b <= x"ff8ff000";
-- Testing expeq, greater and swap bits
--        a <= x"800F345A";
--        b <= x"810F3459";
--        a <= x"000F3456";
--        b <= x"800F3456";
--        a <= x"000F3458";
--        b <= x"800F3456";
-- Testing exponent equalization logic
-- one normalized and one denormalized number
--        a <= x"01540000";
--        b <= x"00680000";
--        a <= x"01D40000";
--        b <= x"00E80000";
--        a <= x"01D40000";
--        b <= x"80E80000";
--        a <= x"81540000";
--        b <= x"00680000";
--        a <= x"81540000";
--        b <= x"01540000";
--        a <= x"3BD57C39";
--        b <= x"5c698edc";
--    a <= x"0659AF67";
--    b <= x"047FC598";
--      a <= x"01800000";
--      b <= x"80400000";
--      a <= x"00800000";
--      b <= x"80400000";
--     a <= x"d85e74fb";
--     b <= x"d06f5a9a";
--     a <= x"ffffffff";
--     b <= x"ffffffff";
--       a <= x"0055a7ff";
--       b <= x"00000001";
    a <= x"607e0000";
    b <= x"e07c0000";
    wait for period; start <= '1';
    wait for period; start <= '0';
    wait for period;
    wait for period; 
    wait for period;
    wait for period;
    wait for period; 
    wait;
end process;


end Behavioral;
