/* Simple C-program to perform addition / subtraction of 
 * 2 single-precision floating point numbers, this is useful
 * for validating the output of the VHDL state machine
 */
#include <stdio.h>

void print_fpversion(int n)
{
    float f = *((float*)&n);
    printf("n is %08x, f is %f\n",n,f);
    return;
}

void print_fpsum(int n, int m)
{
    float f = *((float*)&n);
    float g = *((float*)&m);

    float h = f+g;
    int *x = &h;
    printf("n is %08x, m is %x, f+g is %08x\n",n,m,*x);
    return;
}


int main()
{
    unsigned int n,m;

    // Subtracting a normal number
    // from a subnormal number
    // Ans : 0x81200000
    n = 0x81540000;  // - 1.101 0100 0x0 0x0 0x0 0x0 x 2^(2 - 127)
    m = 0x00680000;  //   0.110 1000 0x0 0x0 0x0 0x0 x 2^(-126)
    print_fpsum(n,m);

    // Ans : 0x019a0000
    n = 0x01D40000;  // 1.101 0100 0x0 0x0 0x0 0x0  x 2^(3 - 127)
    m = 0x80E80000;  // - 1.110 1000 0x0 0x0 0x0 0x0  x 2^(1 - 127)
    print_fpsum(n,m);

    // Ans : 0x5C698ED6
    n = 0x3BD57C39;   // ~ 2^(119-127)
    m = 0x5C698ED6;   // ~ 2^(184-127) : much much larger than 'n'
    print_fpsum(n,m);

    // Ans : 0x0669abc0
    n = 0x0659AF67;   //1.101 1001 1010 1111 0110 0111 x 2^(12 - 127)
    m = 0x047FC598;   //1.111 1111 1100 0101 1001 1000 x 2^(8 - 127)
    print_fpsum(n,m);

    // Subtracting a subnormal number
    // from a normalized number
    // Ans : 0x01600000
    n = 0x01800000;   // 2^(3-127)
    m = 0x80400000;   // -2^-127
    print_fpsum(n,m); // 1.11 x 2^-127

    // Subtracting a subnormal number
    // from a normalized number
    // Ans : 0x00400000
    n = 0x00800000; // 2^-126
    m = 0x80400000; // -2^-127
    print_fpsum(n,m); // 2^-127

    // Adding 2 negative numbers
    // Ans : d85e75ea
    n = 0xD85E74FB;  
    m = 0xD06F5A9A;  
    print_fpsum(n,m);  

    // Adding 2 negative numbers
    // Ans : 
    n = 0xffffffff; 
    m = 0xffffffff; 
    print_fpsum(n,m); 

    //0x0055a800
    n = 0x0055a7ff;
    m = 0x00000001;
    print_fpsum(n,m); 

    n = 0x607E0000;
    m = 0xE07C0000;
    print_fpsum(n,m); 

    return 0;
}
